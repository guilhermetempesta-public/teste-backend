const swaggerAutogen = require("swagger-autogen")();

const outputFile = "./swagger_output.json";
const endpointsFiles = ["./src/routes/index.js"];

const doc = {
  info: {
    title: 'Swagger API',
    version: '1.0.0',
    description: 'Endpoints da documentação em para a realização do teste.',
  },
  host: 'localhost:3000',
  basePath: '/',
  securityDefinitions: {
    bearerAuth: {
      type: 'apiKey',
      name: 'Authorization',
      scheme: 'bearer',
      in: 'header',
    },
  },
  security: [ { bearerAuth: [] } ],
  definitions: {
    Login: {
      $username: "teste",
      $password: "123"
    },
    SignUp: {
      $username: "teste",
      $password: "123",
      $confirmPassword: "123"
    },
    Users: [ 
      {
        id: 1,
        username: "Teste",
        admin: true
      }
    ],
    User: {
      id: 1,
      username: "Teste",
      admin: true
    },
    AddDirector: {
      $name: "Francis Ford Coppola"
    },
    Directors: [ 
      {
        id: 1,
        name: "Francis Ford Coppola",
      }
    ],
    Director: {
      id: 1,
      name: "Francis Ford Coppola",
    },
    AddActor: {
      $name: "Al Pacino"
    },
    Actors: [ 
      {
        id: 1,
        name: "Al Pacino",
      }
    ],
    Actor: {
      id: 1,
      name: "Al Pacino",
    },
    AddGenre: {
      $name: "Drama"
    },
    Genres: [ 
      {
        id: 1,
        name: "Drama",
      }
    ],
    Genre: {
      id: 1,
      name: "Drama",
    },
    Rating: {
      value: 4,
    },
    AddMovie: {
      name: "O Poderoso Chefão",
      releaseYear: 1972,
      synopsis: "O patriarca idoso de uma dinastia do crime organizado transfere o controle de seu império clandestino para seu filho relutante",
      directorId: 2,
      actors: [
        {
          actorId: 5
        },
        {
          actorId: 6
        }
      ],
      genres: [
        {
          genreId: 1
        },
        {
          genreId: 2
        }
      ]
    },
    UpdMovie: {
      name: "O Poderoso Chefão",
      releaseYear: 1972,
      synopsis: "O patriarca idoso de uma dinastia do crime organizado transfere o controle de seu império clandestino para seu filho relutante",
      directorId: 2,
      actors: [
        {	
          id: 1,
          actorId: 5,
          deleted: false
        },
        {
          id: 2,
          actorId: 6,
          deleted: false
        },
        {	
          actorId: 7,
          deleted: false
        }
      ],
      genres: [
        {
          id: 1,
          genreId: 1,
          deleted: false
        },
        {
          id: 1,
          genreId: 2,
          deleted: true
        }
      ]
    },
    Movies: [
      {
        id: 9,
        name: "O Advogado do Diabo",
        releaseYear: 1997,
        averageRating: 3.1
      },
      {
        id: 1,
        name: "O Poderoso Chefão",
        releaseYear: 1972,
        averageRating: 3.77
      }
    ],
    Movie: {
      id: 1,
      name: "O Poderoso Chefão",
      releaseYear: 1972,
      synopsis: "O patriarca idoso de uma dinastia do crime organizado transfere o controle de seu império clandestino para seu filho relutante",
      averageRating: 3.77,
      director: {
        id: 2,
        name: "Francis Ford Coppola"
      },
      actors: [
        {
          id: 1,
          actor: {
            id: 5,
            name: "Marlon Brando"
          }
        },
        {
          id: 5,
          actor: {
            id: 7,
            name: "James Caan"
          }
        },
        {
          id: 2,
          actor: {
            id: 6,
            name: "Al Pacino"
          }
        }
      ],
      "genres": [
        {
          id: 1,
          genre: {
            id: 1,
            name: "Crime"
          }
        },
        {
          id: 2,
          genre: {
            id: 2,
            name: "Drama"
          }
        }
      ]
    },
  }
}

swaggerAutogen(outputFile, endpointsFiles, doc).then(() => {
  require("./server.js");
});
