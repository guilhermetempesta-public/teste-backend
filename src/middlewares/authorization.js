module.exports = (permission) => {
  return (req, res, next) => {
    try { 
      const role = (req.user.admin) ? 'admin' : 'user'; 
      if (permission!=role) {
        return res.status(401).json({
          name: "Unauthorized",
          message: "Acesso não autorizado!",
        });
      }
      next();
    } catch (error) {
      next(error);
    }
  };
};
