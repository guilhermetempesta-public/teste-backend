const { UserController } = require('../controllers/UserController');
const { DirectorController } = require('../controllers/DirectorController');
const { ActorController } = require('../controllers/ActorController');
const { GenreController } = require('../controllers/GenreController');
const { MovieController } = require('../controllers/MovieController');
const authentication = require('../middlewares/authentication');
const authorization = require('../middlewares/authorization');

const user = new UserController();
const director = new DirectorController();
const actor = new ActorController();
const genre = new GenreController();
const movie = new MovieController();

module.exports = app => {

    app.route('/')
      .get((req, res, next)=>{ 
        // #swagger.description = 'Endpoint para testar rota base.'
        res.status(200).json({ message: "Teste backend ioasys" })
      })

    app.route('/signup')
      .post(
        // #swagger.tags = ['User']
        // #swagger.description = 'Endpoint para cadastrar usuários.'
        /* #swagger.parameters['SignUp'] = {
               in: 'body',
               description: 'SignUp.',
               required: true,
               type: 'object',
               schema: { $ref: "#/definitions/SignUp" }
        } */
        user.store)

    app.route('/login')
      .post(authentication.local, 
        // #swagger.tags = ['User']
        // #swagger.description = 'Endpoint para fazer login na api.'
        /* #swagger.parameters['Login'] = {
               in: 'body',
               description: 'Login.',
               required: true,
               type: 'object',
               schema: { $ref: "#/definitions/Login" }
        } */
        user.login)

    app.route('/users')
      .all(authentication.bearer)
      .post(authorization('admin'), 
        // #swagger.tags = ['User']
        // #swagger.description = 'Endpoint para cadastrar usuários.'
        /* #swagger.parameters['SignUp'] = {
               in: 'body',
               description: 'SignUp.',
               required: true,
               type: 'object',
               schema: { $ref: "#/definitions/SignUp" }
        } */
        user.store)
      .get(authorization('admin'), 
        // #swagger.tags = ['User']
        // #swagger.description = 'Endpoint para listar usuários.'
        /* #swagger.responses[200] = { 
               schema: { $ref: "#/definitions/Users" },
               description: 'Usuários encontrados.' 
        } */
        user.index)
    
    app.route('/users/:id')
      .all(authentication.bearer)
      .put(authorization('admin'), 
        // #swagger.tags = ['User']
        // #swagger.description = 'Endpoint para alterar um usuário.'
        // #swagger.parameters['id'] = { description: 'ID do usuário.' }
        /* #swagger.parameters['SignUp'] = {
               in: 'body',
               description: 'SignUp.',
               required: true,
               type: 'object',
               schema: { $ref: "#/definitions/SignUp" }
        } */
        user.update)
      .get(authorization('admin'), 
        // #swagger.tags = ['User']
        // #swagger.description = 'Endpoint para exibir um usuário.'
        // #swagger.parameters['id'] = { description: 'ID do usuário.' }
        /* #swagger.responses[200] = { 
               schema: { $ref: "#/definitions/User" },
               description: 'Usuário encontrado.' 
        } */
        user.show)
      .delete(authorization('admin'), 
        // #swagger.tags = ['User']
        // #swagger.description = 'Endpoint para excluir um usuário.'
        // #swagger.parameters['id'] = { description: 'ID do usuário.' }
        user.destroy)

    app.route('/directors')
      .all(authentication.bearer)
      .post(authorization('admin'), 
        // #swagger.tags = ['Director']
        // #swagger.description = 'Endpoint para cadastrar diretores.'
        /* #swagger.parameters['AddDirector'] = {
              in: 'body',
              description: 'Inserir diretor.',
              required: true,
              type: 'object',
              schema: { $ref: "#/definitions/AddDirector" }
        } */
        director.store)
      .get( 
        // #swagger.tags = ['Director']
        // #swagger.description = 'Endpoint para listar diretores.'
        /* #swagger.responses[200] = { 
               schema: { $ref: "#/definitions/Directors" },
               description: 'Diretores encontrados.' 
        } */
        director.index)
    
    app.route('/directors/:id')
      .all(authentication.bearer)
      .put(authorization('admin'), 
        // #swagger.tags = ['Director']
        // #swagger.description = 'Endpoint para alterar um diretor.'
        // #swagger.parameters['id'] = { description: 'ID do diretor.' }
        /* #swagger.parameters['Director'] = {
              in: 'body',
              description: 'Director.',
              required: true,
              type: 'object',
              schema: { $ref: "#/definitions/AddDirector" }
        } */
        director.update)
      .delete(authorization('admin'),  
        // #swagger.tags = ['Director']
        // #swagger.description = 'Endpoint para excluir um diretor.'
        // #swagger.parameters['id'] = { description: 'ID do diretor.' }
        director.destroy)
      .get(
        // #swagger.tags = ['Director']
        // #swagger.description = 'Endpoint para exibir um diretor.'
        // #swagger.parameters['id'] = { description: 'ID do diretor.' }
        /* #swagger.responses[200] = { 
               schema: { $ref: "#/definitions/Director" },
               description: 'Diretor encontrado.' 
        } */
        director.show)

    app.route('/actors')
      .all(authentication.bearer)
      .post(authorization('admin'), 
        // #swagger.tags = ['Actor']
        // #swagger.description = 'Endpoint para cadastrar atores.'
        /* #swagger.parameters['AddActor'] = {
              in: 'body',
              description: 'Inserir ator.',
              required: true,
              type: 'object',
              schema: { $ref: "#/definitions/AddActor" }
        } */
        actor.store)
      .get(
        // #swagger.tags = ['Actor']
        // #swagger.description = 'Endpoint para listar atores.'
        /* #swagger.responses[200] = { 
               schema: { $ref: "#/definitions/Actors" },
               description: 'Atores encontrado.' 
        } */
        actor.index)
    
    app.route('/actors/:id')
      .all(authentication.bearer)
      .put(authorization('admin'),
        // #swagger.tags = ['Actor']
        // #swagger.description = 'Endpoint para alterar um ator.'
        // #swagger.parameters['id'] = { description: 'ID do ator.' }
        /* #swagger.parameters['Actor'] = {
              in: 'body',
              description: 'Actor.',
              required: true,
              type: 'object',
              schema: { $ref: "#/definitions/AddActor" }
        } */
        actor.update)
      .delete(authorization('admin'),  
        // #swagger.tags = ['Actor']
        // #swagger.description = 'Endpoint para excluir um ator.'
        // #swagger.parameters['id'] = { description: 'ID do ator.' }
        actor.destroy)     
      .get(
        // #swagger.tags = ['Actor']
        // #swagger.description = 'Endpoint para exibir um ator.'
        // #swagger.parameters['id'] = { description: 'ID do ator.' }
        /* #swagger.responses[200] = { 
               schema: { $ref: "#/definitions/Actor" },
               description: 'Ator encontrado.' 
        } */
        actor.show)
      
    app.route('/genres')
      .all(authentication.bearer)
      .post(authorization('admin'), 
        // #swagger.tags = ['Genre']
        // #swagger.description = 'Endpoint para cadastrar gêneros.'
        /* #swagger.parameters['AddGenre'] = {
              in: 'body',
              description: 'Inserir gênero.',
              required: true,
              type: 'object',
              schema: { $ref: "#/definitions/AddGenre" }
        } */
        genre.store)
      .get(
        // #swagger.tags = ['Genre']
        // #swagger.description = 'Endpoint para exibir um gênero.'
        // #swagger.parameters['id'] = { description: 'ID do gênero.' }
        /* #swagger.responses[200] = { 
               schema: { $ref: "#/definitions/Genre" },
               description: 'Gêneros encontrados.' 
        } */
        genre.index)
    
    app.route('/genres/:id')
      .all(authentication.bearer)
      .put(authorization('admin'),
        // #swagger.tags = ['Genre']
        // #swagger.description = 'Endpoint para alterar um gênero.'
        // #swagger.parameters['id'] = { description: 'ID do gênero.' }
        /* #swagger.parameters['Genre'] = {
              in: 'body',
              description: 'Genre.',
              required: true,
              type: 'object',
              schema: { $ref: "#/definitions/AddGenre" }
        } */
        genre.update)
      .delete(authorization('admin'),  
        // #swagger.tags = ['Genre']
        // #swagger.description = 'Endpoint para excluir um gênero.'
        // #swagger.parameters['id'] = { description: 'ID do gênero.' }
        genre.destroy)    
      .get(
        // #swagger.tags = ['Genre']
        // #swagger.description = 'Endpoint para listar generos.'
        /* #swagger.responses[200] = { 
               schema: { $ref: "#/definitions/Genre" },
               description: 'Gênero encontrado.' 
        } */
        genre.show)
      
    app.route('/movies')
      .all(authentication.bearer)
      .post(authorization('admin'),
        // #swagger.tags = ['Movie']
        // #swagger.description = 'Endpoint para cadastrar filmes.'
        /* #swagger.parameters['AddMovie'] = {
              in: 'body',
              description: 'Inserir filme.',
              required: true,
              type: 'object',
              schema: { $ref: "#/definitions/AddMovie" }
        } */
        movie.store)
      .get(
        // #swagger.tags = ['Movie']
        // #swagger.description = 'Endpoint para listar filmes.'
        /* #swagger.responses[200] = { 
               schema: { $ref: "#/definitions/Movie" },
               description: 'Filmes encontrados.' 
        } */
        movie.index)
    
    app.route('/movies/:id')
      .all(authentication.bearer)
      .put(authorization('admin'),
        // #swagger.tags = ['Movie']
        // #swagger.description = 'Endpoint para alterar um filme.'
        // #swagger.parameters['id'] = { description: 'ID do filme.' }
        /* #swagger.parameters['Movie'] = {
              in: 'body',
              description: 'Movie.',
              required: true,
              type: 'object',
              schema: { $ref: "#/definitions/UpdMovie" }
        } */
        movie.update)
      .delete(authorization('admin'),  
        // #swagger.tags = ['Movie']
        // #swagger.description = 'Endpoint para excluir um filme.'
        // #swagger.parameters['id'] = { description: 'ID do filme.' }
        movie.destroy)
      .get(
        // #swagger.tags = ['Movie']
        // #swagger.description = 'Endpoint para exibir um filme.'
        // #swagger.parameters['id'] = { description: 'ID do filme.' }
        /* #swagger.responses[200] = { 
               schema: { $ref: "#/definitions/Movie" },
               description: 'Filme encontrado.' 
        } */
        movie.show)

    app.route('/movies/:id/rating')
      .all(authentication.bearer)
      .post(authorization('user'),
        // #swagger.tags = ['Movie']
        // #swagger.description = 'Endpoint para avaliar um filme.'
        // #swagger.parameters['id'] = { description: 'ID do filme.' }
        /* #swagger.parameters['Rating'] = {
              in: 'body',
              description: 'Rating.',
              required: true,
              type: 'object',
              schema: { $ref: "#/definitions/Rating" }
        } */
        movie.rating)
}