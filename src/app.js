require("dotenv").config();
require("./database/index.js");

const express = require("express");
const bodyParser = require("body-parser");
const cors = require('cors');
const corsOptions = {
    exposedHeaders: 'Authorization',
};

const swaggerUi = require('swagger-ui-express')
const swaggerFile = require('../swagger_output.json')

const app = express();
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors(corsOptions));

require('./auth/strategies');
const errorHandler = require("./middlewares/errorHandler.js");

const routes = require("./routes");
routes(app);

app.use('/doc', swaggerUi.serve, swaggerUi.setup(swaggerFile));

app.use(errorHandler);

module.exports = app;
