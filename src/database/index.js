const fs = require("fs");
const path = require("path");
const Sequelize = require("sequelize");
const env = process.env.NODE_ENV || "development";
const config = require("../config/database.js")[env];

const models = {};
const modelsDir = path.join(__dirname, "../models");

const sequelize = new Sequelize(config.url, config);

fs.readdirSync(modelsDir).forEach((file) => {
  const model = require(path.join(modelsDir, file));
  models[model.name] = model;
});

Object.keys(models).forEach((model) => {
  models[model].init(sequelize);
});

Object.keys(models).forEach((model) => {
  if (models[model].associate) {
    models[model].associate(models);
  }
});

models.sequelize = sequelize;
models.Sequelize = Sequelize;

module.exports = sequelize;
