const { Model, DataTypes } = require("sequelize");

class Genre extends Model {
  static init(sequelize) {
    super.init(
      {
        id: {
          type: DataTypes.INTEGER,
					autoIncrement: true,
          primaryKey: true,
        },
        name: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            notNull: { msg: "Nome não informado!" },
            notEmpty: { msg: "Nome não informado!" },
          },
        }
      },
      {
        sequelize,
        tableName: "genres",
        paranoid: true
      }
    );
  }

  static associate(models) {
    this.hasMany(models.MovieGenre, { foreignKey: 'genreId', as: 'movies' });        
  }
}

module.exports = Genre;
