const { Model, DataTypes } = require("sequelize");

class MovieGenre extends Model {
  static init(sequelize) {
    super.init(
      {},
      {
        sequelize,
        tableName: "movies_genres",
        paranoid: true,
      }
    );
  }

  static associate(models) {
    this.belongsTo(models.Movie, { foreignKey: "movieId", as: "movie" });
    this.belongsTo(models.Genre, { foreignKey: "genreId", as: "genre" });
  }
}

module.exports = MovieGenre;
