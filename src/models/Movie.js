const { Model, DataTypes } = require("sequelize");

class Movie extends Model {
  static init(sequelize) {
    super.init(
      {
        name: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            notNull: { msg: "Nome não informado!" },
            notEmpty: { msg: "Nome não informado!" },
          },
        },
        releaseYear: {
          type: DataTypes.INTEGER,
          allowNull: false,
          validate: {
            notNull: { msg: "Ano de lançamento não informado!" },
            notEmpty: { msg: "Ano de lançamento não informado!" },
          },
        },
        synopsis: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            notNull: { msg: "Sinopse não informada!" },
            notEmpty: { msg: "Sinopse não informada!" },
          },
        }
      },
      {
        sequelize,
        tableName: "movies",
        paranoid: true
      }
    );
  }  

  static associate(models) {
    this.hasMany(models.MovieActor, { foreignKey: "movieId", as: "actors" });
    this.hasMany(models.MovieGenre, { foreignKey: "movieId", as: "genres" });
    this.belongsTo(models.Director, { foreignKey: "directorId", as: "director" });
    this.hasMany(models.Ratings, { foreignKey: "movieId", as: "ratings" });
  }
}

module.exports = Movie;
