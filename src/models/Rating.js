const { Model, DataTypes } = require("sequelize");

class Ratings extends Model {
  static init(sequelize) {
    super.init(
      {
        value: {
          type: DataTypes.NUMERIC,
          allowNull: false,
          validate: {
            min: 0,
            max: 4,
          },
        },
      },
      {
        sequelize,
        tableName: "ratings",
        paranoid: true,
      }
    );
  }

  static associate(models) {
    this.belongsTo(models.Movie, { foreignKey: "movieId", as: "movie" });
    this.belongsTo(models.Actor, { foreignKey: "userId", as: "user" });
  }
}

module.exports = Ratings;
