const { Model, DataTypes } = require("sequelize");

class MovieActor extends Model {
  static init(sequelize) {
    super.init(
      {},
      {
        sequelize,
        tableName: "movies_actors",
        paranoid: true,
      }
    );
  }

  static associate(models) {
    this.belongsTo(models.Movie, { foreignKey: "movieId", as: "movie" });
    this.belongsTo(models.Actor, { foreignKey: "actorId", as: "actor" });
  }
}

module.exports = MovieActor;
