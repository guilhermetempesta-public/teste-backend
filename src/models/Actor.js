const { Model, DataTypes } = require("sequelize");

class Actor extends Model {
  static init(sequelize) {
    super.init(
      {
        id: {
          type: DataTypes.INTEGER,
					autoIncrement: true,
          primaryKey: true,
        },
        name: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            notNull: { msg: "Nome não informado!" },
            notEmpty: { msg: "Nome não informado!" },
          },
        }
      },
      {
        sequelize,
        tableName: "actors",
        paranoid: true
      }
    );
  }

  static associate(models) {
    this.hasMany(models.MovieActor, { foreignKey: 'actorId', as: 'movies' });        
  }
}

module.exports = Actor;
