const { Model, DataTypes } = require("sequelize");
const bcrypt = require("bcrypt-nodejs");

class Director extends Model {
  static init(sequelize) {
    super.init(
      {
        id: {
          type: DataTypes.INTEGER,
					autoIncrement: true,
          primaryKey: true,
        },
        name: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            notNull: { msg: "Nome não informado!" },
            notEmpty: { msg: "Nome não informado!" },
          },
        }
      },
      {
        sequelize,
        tableName: "directors",
        paranoid: true
      }
    );
  }

  static associate(models) {}
}

module.exports = Director;
