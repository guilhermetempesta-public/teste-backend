const { Model, DataTypes } = require("sequelize");
const bcrypt = require("bcrypt-nodejs");

class User extends Model {
  static init(sequelize) {
    super.init(
      {
        id: {
          type: DataTypes.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        username: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            notNull: { msg: "Nome de usuário não informado!" },
            notEmpty: { msg: "Nome de usuário não informado!" },
          },
        },
        password: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            notNull: { msg: "Senha não informada!" },
            notEmpty: { msg: "Senha não informada!" },
          },
        },
        confirmPassword: {
          type: DataTypes.VIRTUAL,
          allowNull: false,
          validate: {
            notNull: { msg: "Confirme a senha!" },
            notEmpty: { msg: "Confirme a senha!" },
            isMatch: function () {
              const matched = bcrypt.compareSync(
                this.confirmPassword,
                this.password
              );
              if (!matched) {
                throw new Error("As senhas não conferem!");
              }
            },
          },
        },
        admin: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
        },
      },
      {
        sequelize,
        tableName: "users",
        paranoid: true,
        setterMethods: {
          password: function (value) {
            const salt = bcrypt.genSaltSync(10);
            const encryptedPassword = bcrypt.hashSync(value, salt);
            this.setDataValue("password", encryptedPassword);
          },
        },
      }
    );

    User.checkPassword = (password, encryptPassword) => {
      const { InvalidCredentialsError } = require("../utils/errors");
      const matched = bcrypt.compareSync(password, encryptPassword);
      if (!matched) throw new InvalidCredentialsError();
    };
  }

  static associate(models) {
    this.hasMany(models.Ratings, { foreignKey: "userId", as: "ratings" });
  }
}

module.exports = User;
