require('dotenv').config();

module.exports = {
  "development": {
    "dialect": "postgres",
    "url": process.env.DATABASE_URL,
    "define": {
      "timestamps": true,
      "underscored": true
    },
    "seederStorage": "sequelize"
  },
  "production": {
    "dialect": "postgres",
    "url": process.env.DATABASE_URL,
    "dialectOptions": {
      "ssl": {
        "require": true,
        "rejectUnauthorized": false
      }
    },
    "define": {
      "timestamps": true,
      "underscored": true
    },
    "seederStorage": "sequelize"
  }
}