const Actor = require("../models/Actor");
const { NotFoundError } = require("../utils/errors");

class ActorController {
  async store(req, res, next) {
    try {
      const data = req.body;
      await Actor.create(data);
      res.status(201).json({ message: "Ator cadastrado com sucesso!" });
    } catch (error) {
      next(error);
    }
  }

  async update(req, res, next) {
    try {
      const data = req.body;
      await Actor.update(data, {
        where: { id: req.params.id },
      });
      res.status(200).json({ message: "Ator atualizado com sucesso." });
    } catch (error) {
      next(error);
    }
  }

  async index(req, res, next) {
    try {
      const actors = await Actor.findAll({
        attributes: ["id", "name"],
        order: [["id"]],
      });
      res.status(200).json(actors);
    } catch (error) {
      next(error);
    }
  }

  async show(req, res, next) {
    try {
      const actor = await Actor.findOne({
        attributes: ["id", "name"],
        where: { id: req.params.id },
      });
      if (!actor) throw new NotFoundError("Ator não encontrado!");
      res.status(200).json(actor);
    } catch (error) {
      next(error);
    }
  }

  async destroy(req, res, next) {
    try {
      const deleted = await Actor.destroy({
        where: { id: req.params.id },
      });
      if (deleted === 0) throw new NotFoundError("Ator não encontrado!");
      res.status(200).send({ message: "Ator excluído com sucesso." });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = { ActorController };