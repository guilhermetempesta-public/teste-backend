const Movie = require("../models/Movie");
const MovieActor = require("../models/MovieActor");
const MovieGenre = require("../models/MovieGenre");
const Rating = require("../models/Rating");
const { Sequelize, Op } = require('sequelize');
const { NotFoundError, InvalidArgumentError } = require("../utils/errors");

class MovieController {
  async store(req, res, next) {
    try {
      const movie = req.body;
      await Movie.sequelize.transaction(async (t) => {
        await Movie.create(movie, {
          include: [{ association: "actors" }, { association: "genres" }],
          transaction: t,
        });
      });

      res.status(201).json({ message: "Cadastrado realizado com sucesso!" });
    } catch (error) {
      next(error);
    }
  }

  async update(req, res, next) {
    try {
      const data = req.body;
      const { actors, genres, ...movie } = data;
      movie.id = req.params.id;

      const movieFromDB = await Movie.findByPk(req.params.id, {
        include: [{ association: "actors" }, { association: "genres" }],
      });
      if (!movieFromDB) {
        throw new NotFoundError("Nenhuma informação foi encontrada!");
      }

      const actorsFromDB = movieFromDB.actors;
      const genresFromDB = movieFromDB.genres;

      await Movie.sequelize.transaction(async (t) => {
        await actors.map(async (actor) => {
          const found = await actorsFromDB.find(
            (_actor) => _actor.id === actor.id
          );
          if (found) {
            if (actor.deleted) {
              await MovieActor.destroy({
                where: { id: actor.id },
                transaction: t,
              });
            }
          } else {
            delete actor.id;
            actor.movieId = movie.id;
            await MovieActor.create(actor, {
              transaction: t,
            });
          }
        });

        await genres.map(async (genre) => {
          const found = await genresFromDB.find(
            (_genre) => _genre.id === genre.id
          );
          if (found) {
            if (genre.deleted) {
              await MovieGenre.destroy({
                where: { id: genre.id },
                transaction: t,
              });
            }
          } else {
            delete genre.id;
            genre.movieId = movie.id;
            await MovieGenre.create(genre, {
              transaction: t,
            });
          }
        });

        const [updated] = await Movie.update(movie, {
          where: { id: movie.id },
          transaction: t,
        });
        return updated;
      });

      res.status(200).json({ message: "Informações atualizadas com sucesso!" });
    } catch (error) {
      next(error);
    }
  }

  async indexA (req, res, next) {
    try {
      const filters = queryFilter(req.query);

      const moviesFromDB = await Movie.findAll({
        attributes: [
          "id",
          "name",
          "releaseYear",
          [
            Sequelize.fn("AVG", Sequelize.col("ratings.value")),
            "averageRating",
          ],
        ],
        include: [
          {
            association: "ratings",
            attributes: [],
          },
        ],
        where: filters,
        group: ['"Movie".id'],
        order: ["name"],
      });

      const movies = moviesFromDB.map((movie) => {
        const a = movie.toJSON();
        a.averageRating = Math.round(a.averageRating * 100) / 100;
        return a;
      });

      res.status(200).json(movies);
    } catch (error) {
      next(error);
    }
  }

  async index (req, res, next) {
    try {
      const filters = queryFilter(req.query);

      const moviesFromDB = await Movie.findAll({
        attributes: [
          "id",
          "name",
          "releaseYear",
          [
            Sequelize.fn("AVG", Sequelize.col("ratings.value")),
            "averageRating",
          ],
        ],
        include: [{
            association: "ratings",
            attributes: [],
          },{
            association: "genres",
            attributes: [],
          },{
            association: "actors",
            attributes: [],
          },{
            association: "director",
            attributes: [],
          },
        ],
        where: filters,
        group: ['"Movie".id'],
        order: ["name"],
      });

      const movies = moviesFromDB.map((movie) => {
        const a = movie.toJSON();
        a.averageRating = Math.round(a.averageRating * 100) / 100;
        return a;
      });

      res.status(200).json(movies);
    } catch (error) {
      next(error);
    }
  }

  async show(req, res, next) {
    try {
      const id = req.params.id;

      let movie = await Movie.findOne({
        attributes: [
          "id",
          "name",
          "releaseYear",
          "synopsis",
          [
            Sequelize.fn("AVG", Sequelize.col("ratings.value")),
            "averageRating",
          ],
        ],
        include: [
          {
            association: "ratings",
            attributes: [],
          },
          {
            association: "director",
            attributes: ["id", "name"],
          },
          {
            association: "actors",
            attributes: ["id"],
            include: {
              association: "actor",
              attributes: ["id", "name"],
            },
          },
          {
            association: "genres",
            attributes: ["id"],
            required: true,
            include: {
              association: "genre",
              attributes: ["id", "name"],
            },
          },
        ],
        where: { id: id },
        group: [
          '"Movie".id',
          "director.id",
          "actors.id",
          "actors->actor.id",
          "genres.id",
          "genres->genre.id",
        ],
        order: ["name"],
      });

      if (!movie) throw new NotFoundError("Filme não encontrado!");

      movie = movie.toJSON();
      movie.averageRating = Math.round(movie.averageRating * 100) / 100;

      res.status(200).json(movie);
    } catch (error) {
      next(error);
    }
  }

  async destroy(req, res, next) {
    try {
      await Movie.destroy({
        where: { id: req.params.id },
      });
      res.status(200).json({ message: "Exclusão realizada com sucesso!" });
    } catch (error) {
      next(error);
    }
  }

  async rating(req, res, next) {
    try {
      const userId = req.user.id;

      const movieId = req.params.id;
      const movie = await Movie.findByPk(movieId);
      if (!movie) throw new NotFoundError("Filme não encontrado!");

      const value = req.body.value;
      if (!value || value < 0 || value > 4)
        throw new InvalidArgumentError(
          "Valor informado inválido! Informe um valor entre 0 e 4."
        );

      const rating = { userId, movieId, value };
      const movieRating = await Rating.findOne({
        where: {
          userId: userId,
          movieId: movieId,
        },
      });
      if (!movieRating) {
        await Rating.create(rating);
      } else {
        await Rating.update(rating, { where: { id: movieRating.id } });
      }

      res.status(201).json({ message: "Avaliação realizada com sucesso!" });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = { MovieController };

function queryFilter (queryParams) {
  let filters = {};
  const { directorId, actorId, genreId, name } = queryParams;
  
  if (directorId) {
    filters = {...filters, '$director.id$': directorId };
  }

  if (actorId) {
    filters = {...filters, '$actors.actor_id$': actorId };
  }

  if (genreId) {
    filters = {...filters, '$genres.genre_id$': genreId };
  }

  if (name) {
    filters = {...filters, name: {[Op.iLike]: `%${name}%`} };
  }

  return filters
}
