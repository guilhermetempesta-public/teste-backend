const Genre = require("../models/Genre");
const { NotFoundError } = require("../utils/errors");

class GenreController {
  async store(req, res, next) {
    try {
      const data = req.body;
      await Genre.create(data);
      res.status(201).json({ message: "Gênero cadastrado com sucesso!" });
    } catch (error) {
      next(error);
    }
  }

  async update(req, res, next) {
    try {
      const data = req.body;
      await Genre.update(data, {
        where: { id: req.params.id },
      });
      res.status(200).json({ message: "Gênero atualizado com sucesso." });
    } catch (error) {
      next(error);
    }
  }

  async index(req, res, next) {
    try {
      const genres = await Genre.findAll({
        attributes: ["id", "name"],
        order: [["id"]],
      });
      res.status(200).json(genres);
    } catch (error) {
      next(error);
    }
  }

  async show(req, res, next) {
    try {
      const genre = await Genre.findOne({
        attributes: ["id", "name"],
        where: { id: req.params.id },
      });
      if (!genre) throw new NotFoundError("Gênero não encontrado!");
      res.status(200).json(genre);
    } catch (error) {
      next(error);
    }
  }

  async destroy(req, res, next) {
    try {
      const deleted = await Genre.destroy({
        where: { id: req.params.id },
      });
      if (deleted === 0) throw new NotFoundError("Gênero não encontrado!");
      res.status(200).send({ message: "Gênero excluído com sucesso." });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = { GenreController };