const User = require("../models/User");
const tokens = require("../auth/tokens");
const { InvalidArgumentError, NotFoundError } = require("../utils/errors");

class UserController {
  async store(req, res, next) {
    try {
      const data = req.body;
      if (!req.originalUrl.startsWith("/users")) {
        const count = await User.count();
        data.admin = (count===0);
      }
      const userExists = await User.findOne({
        where: { username: data.username },
      });
      if (userExists)
        throw new InvalidArgumentError(
          "Este nome de usuário já foi utilizado!"
        );
      await User.create(data);
      res.status(201).json({ message: "Usuário cadastrado com sucesso!" });
    } catch (error) {
      next(error);
    }
  }

  async update(req, res, next) {
    try {
      const data = req.body;
      await User.update(data, {
        where: { id: req.params.id },
      });
      res.status(200).json({ message: "Usuário atualizado com sucesso." });
    } catch (error) {
      next(error);
    }
  }

  async index(req, res, next) {
    try {
      const users = await User.findAll({
        attributes: ["id", "username", "admin"],
        order: [["admin"]],
      });
      res.status(200).json(users);
    } catch (error) {
      next(error);
    }
  }

  async show(req, res, next) {
    try {
      const user = await User.findOne({
        attributes: ["id", "username", "admin"],
        where: { id: req.params.id },
      });
      if (!user) throw new NotFoundError("Usuário não encontrado!");
      res.status(200).json(user);
    } catch (error) {
      next(error);
    }
  }

  async destroy(req, res, next) {
    try {
      const deleted = await User.destroy({
        where: { id: req.params.id },
      });
      if (deleted === 0) throw new NotFoundError("Usuário não encontrado!");
      res.status(200).send({ message: "Usuário excluído com sucesso." });
    } catch (error) {
      next(error);
    }
  }

  async login(req, res, next) {    
    try {
      const token = tokens.access.create(req.user);
      res.set("Authorization", token);
      res.status(200).json({ message: "Usuário autenticado." });
    } catch (error) {
      next(error);
    }
  } 
}

module.exports = { UserController };