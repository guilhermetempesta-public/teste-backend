const Director = require("../models/Director");
const { NotFoundError } = require("../utils/errors");

class DirectorController {
  async store(req, res, next) {
    try {
      const data = req.body;
      await Director.create(data);
      res.status(201).json({ message: "Diretor cadastrado com sucesso!" });
    } catch (error) {
      next(error);
    }
  }

  async update(req, res, next) {
    try {
      const data = req.body;
      await Director.update(data, {
        where: { id: req.params.id },
      });
      res.status(200).json({ message: "Diretor atualizado com sucesso." });
    } catch (error) {
      next(error);
    }
  }

  async index(req, res, next) {
    try {
      const directors = await Director.findAll({
        attributes: ["id", "name"],
        order: [["id"]],
      });
      res.status(200).json(directors);
    } catch (error) {
      next(error);
    }
  }

  async show(req, res, next) {
    try {
      const director = await Director.findOne({
        attributes: ["id", "name"],
        where: { id: req.params.id },
      });
      if (!director) throw new NotFoundError("Diretor não encontrado!");
      res.status(200).json(director);
    } catch (error) {
      next(error);
    }
  }

  async destroy(req, res, next) {
    try {
      const deleted = await Director.destroy({
        where: { id: req.params.id },
      });
      if (deleted === 0) throw new NotFoundError("Diretor não encontrado!");
      res.status(200).send({ message: "Diretor excluído com sucesso." });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = { DirectorController };