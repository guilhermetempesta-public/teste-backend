const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy;
const BearerStrategy = require('passport-http-bearer').Strategy;
const User = require('../models/User');
const { InvalidCredentialsError } = require('../utils/errors');
const tokens = require('../auth/tokens'); 

passport.use(
  new LocalStrategy({
      usernameField: 'username',
      passwordField: 'password',
      session: false
    }, 
    async (username, password, done) => {
      try {
        const user = await User.findOne({
          where: { username: username }
        });
        if (!user) { throw new InvalidCredentialsError }
        User.checkPassword(password, user.password);        
        done(null, user);
      } catch (error) {
        done(error);
      }
    }
  )
);

passport.use(
  new BearerStrategy(
    async (token, done) => {
      try {
        const payload = await tokens.access.check(token);
        done(null, { ...payload }, { token: token });
      } catch (error) {
        done(error);
      }      
    }
  )
);