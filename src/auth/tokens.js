const authSecret = process.env.JWT_SECRET;
const jwt = require("jsonwebtoken");

function createJwtToken(user, expiration) {
  const now = Math.floor(Date.now() / 1000); 
  const expirationDate = now + expiration;   

  const payload = {
    id: user.id,
    username: user.username,
    admin: user.admin,
    iat: now,
    exp: expirationDate,
  };

  const token = jwt.sign(payload, authSecret);
  return token;
}

async function checkJwtToken(token) {
  const payload = jwt.verify(token, authSecret);
  return payload;
}

module.exports = {
  access: {
    expiration: 60 * 60 * 24 * 1,
    create(user) {
      return createJwtToken(user, this.expiration);
    },
    check(token) {
      return checkJwtToken(token);
    },
  },
};
