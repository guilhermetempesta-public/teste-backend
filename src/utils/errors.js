class InvalidArgumentError extends Error {
  constructor(message) {
    super(message);
    this.name = 'InvalidArgumentError';
  }
}

class BadRequestError extends Error {
  constructor(message) {
    super(message);
    this.name = 'BadRequestError';
  }
}

class MethodNotAllowedError extends Error {
  constructor(message, replaceDefault) {
    const defaultMessage = 'Operação não permitida!';
    if (!message) { 
      message = defaultMessage 
    } else {
      if (!replaceDefault) message = `${defaultMessage} ${message}`   
    }       
    super(message);
    this.name = 'MethodNotAllowedError';
  }
}

class NotFoundError extends Error {
  constructor (message) {
    const defaultMessage = 'Registro não encontrado!'
    if (!message) { message = defaultMessage }
    super(message)
    this.name = 'NotFoundError'
  }
}

class UnauthorizedError extends Error {
  constructor () {
    const message = 'Acesso não autorizado!'
    super(message)
    this.name = 'UnauthorizedError'
  }
} 

class InvalidCredentialsError extends Error {
  constructor () {
    const message = 'Credenciais inválidas!'
    super(message)
    this.name = 'InvalidCredentialsError'
  }
}

module.exports = {
  InvalidArgumentError, 
  MethodNotAllowedError,
  BadRequestError, 
  UnauthorizedError, 
  NotFoundError, 
  InvalidCredentialsError 
};